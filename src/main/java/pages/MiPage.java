package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class MiPage extends ProjectMethods{
	
	public MiPage() {
		PageFactory.initElements(driver, this);

	}

	public MiPage verifyMiPageTitle() {
		
		WebDriverWait wait = new WebDriverWait(driver, 60);

		wait.until(ExpectedConditions
				.titleContains("Mi Mobile Phones"));
		String MiPageTitle = driver.getTitle();
		System.out.println(MiPageTitle);
		if (MiPageTitle.equals("Mi Mobile Phones"))
			System.out.println("Page title correct");
		else
			System.out.println("PageTitle incorrect");

		
		return this;
	}
	
	@FindBy(xpath = "//div[text()='Newest First']") WebElement newest;
	public ProductsPage clickNewest() throws Exception{
		
		//WebElement newest = driver.findElement(By.xpath("//div[text()='Newest First']"));
		newest.click();

		Thread.sleep(5000);
		
		return new ProductsPage();
	}
}
