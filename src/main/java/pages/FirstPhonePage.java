package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FirstPhonePage extends ProjectMethods{
	
	public FirstPhonePage() {
		PageFactory.initElements(driver, this);
	}
	
	public FirstPhonePage clickFirstPhone() {
		String mobileModelName = driver.findElementByXPath("//div[text()='Newest First']/following::div[@class='_3wU53n']").getText();
		driver.findElementByXPath("//div[text()='Newest First']/following::div[@class='_3wU53n']").click();
		return this ;
	}
public Review_RatingsPage verifyFirstPhonePageTitle() throws Exception {
	Set<String> allWindows = driver.getWindowHandles();
	List<String> lst= new ArrayList<String>();
	lst.addAll(allWindows);
	driver.switchTo().window(lst.get(1));
	
	String firstPhTitle = driver.findElement(By.xpath("//span[@class='_35KyD6']")).getText();
	System.out.println(firstPhTitle);
	
	Thread.sleep(5000);
	
	String Newitemtile = driver.getTitle();
	System.out.println(Newitemtile);
	
	if(Newitemtile.contains("Redmi 6 Pro (Lake Blue, 64 GB)")) System.out.println("Title Correct");
	else System.out.println("Incorrect Title");
	
	
	return new Review_RatingsPage();

}

}
