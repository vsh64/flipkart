package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath ="//span[text()='Electronics']") WebElement Elec;
	public ElectronicsPage clickElectronics() {
		

		//WebElement Elec = driver.findElement(By.xpath("//span[text()='Electronics']"));
		click(Elec);

		Actions ac = new Actions(driver);
		ac.moveToElement(Elec).perform();
		
		return new ElectronicsPage();

	}

}
