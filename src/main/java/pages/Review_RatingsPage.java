package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Review_RatingsPage extends ProjectMethods{

	
	public Review_RatingsPage() {
		PageFactory.initElements(driver, this);
	}
	
	public Review_RatingsPage CountRvwAndRvw() {

		String Review_Ratings = driver.findElement(By.xpath("//span[@class='_35KyD6']/following::span[2]")).getText();
		System.out.println(Review_Ratings);
		driver.quit();
		return this;
		
	}
}
