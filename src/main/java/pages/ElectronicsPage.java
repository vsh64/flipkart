package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class ElectronicsPage  extends ProjectMethods{
	
	public ElectronicsPage() {
		PageFactory.initElements(driver, this);
			}
@FindBy(xpath="(//a[text()='Mi'])[1]") WebElement mi;
	public MiPage clickMi() {
		

		//WebElement mi = driver.findElement(By.xpath("(//a[text()='Mi'])[1]"));

		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(mi));
		mi.click();
		return new MiPage();
	}
}
