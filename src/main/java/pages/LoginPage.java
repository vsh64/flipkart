package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {
	
	public LoginPage(){	
		PageFactory.initElements(driver, this);
		
	}

	public HomePage closeLogin() {
		
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		
		return new HomePage();
		
		
	}
}
