package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class ProductsPage extends ProjectMethods {

	public ProductsPage() {
		PageFactory.initElements(driver, this);
	}

	public FirstPhonePage getProducts() {
		WebDriverWait wait = new WebDriverWait(driver, 60);

		List<WebElement> products = driver.findElements(By.xpath("//div[@class='_3wU53n']"));

		List<WebElement> price = driver.findElements(By.xpath("//div[@class='_1vC4OE _2rQ-NK']"));
		wait.until(ExpectedConditions.visibilityOfAllElements(products));
		for (int i = 0; i < products.size(); i++) {
			System.out.println(products.get(i).getText() + " for " + price.get(i).getText());
		}

		return new FirstPhonePage();
	}
	
	
}

